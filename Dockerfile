FROM atlas/analysisbase:21.2.96
ADD . /code
RUN sudo sh -c "source /home/atlas/release_setup.sh && pip install hftools"

# FROM atlas/analysisbase:21.2.96
# ADD . /analysis/src
# WORKDIR /analysis/build
# RUN source ~/release_setup.sh && \
#  sudo chown -R atlas /analysis && \
#  cmake ../src && \
#  make -j4
